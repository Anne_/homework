//service_button click => service_what opacity=1
let web_design_button = document.getElementById("web_design_button");
let graphic_design_button = document.getElementById("graphic_design_button");
let online_support_button = document.getElementById("online_support_button");
let app_design_button = document.getElementById("app_design_button");
let online_marketing_button = document.getElementById("online_marketing_button");
let seo_service_button = document.getElementById("seo_service_button");

let web_design_div = document.getElementById("web_design_div");
let graphic_design_div = document.getElementById("graphic_design_div");
let online_support_div = document.getElementById("online_support_div");
let app_design_div = document.getElementById("app_design_div");
let online_marketing_div = document.getElementById("online_marketing_div");
let seo_service_div = document.getElementById("seo_service_div");

web_design_button.addEventListener("click",()=>{
    web_design_div.style.opacity = 1;
    graphic_design_div.style.opacity = 0;
    online_support_div.style.opacity = 0;
    app_design_div.style.opacity = 0;
    online_marketing_div.style.opacity = 0;
    seo_service_div.style.opacity = 0;
});
graphic_design_button.addEventListener("click",()=>{
    web_design_div.style.opacity = 0;
    graphic_design_div.style.opacity = 1;
    online_support_div.style.opacity = 0;
    app_design_div.style.opacity = 0;
    online_marketing_div.style.opacity = 0;
    seo_service_div.style.opacity = 0;
});
online_support_button.addEventListener("click",()=>{
    web_design_div.style.opacity = 0;
    graphic_design_div.style.opacity = 0;
    online_support_div.style.opacity = 1;
    app_design_div.style.opacity = 0;
    online_marketing_div.style.opacity = 0;
    seo_service_div.style.opacity = 0;
});
app_design_button.addEventListener("click",()=>{
    web_design_div.style.opacity = 0;
    graphic_design_div.style.opacity = 0;
    online_support_div.style.opacity = 0;
    app_design_div.style.opacity = 1;
    online_marketing_div.style.opacity = 0;
    seo_service_div.style.opacity = 0;
});
online_marketing_button.addEventListener("click",()=>{
    web_design_div.style.opacity = 0;
    graphic_design_div.style.opacity = 0;
    online_support_div.style.opacity = 0;
    app_design_div.style.opacity = 0;
    online_marketing_div.style.opacity = 1;
    seo_service_div.style.opacity = 0;
});
seo_service_button.addEventListener("click",()=>{
    web_design_div.style.opacity = 0;
    graphic_design_div.style.opacity = 0;
    online_support_div.style.opacity = 0;
    app_design_div.style.opacity = 0;
    online_marketing_div.style.opacity = 0;
    seo_service_div.style.opacity = 1;
});
let all_work_button = document.getElementById("all_work_button");
let second_twelve = document.getElementsByClassName("second_twelve");
let graphic_design_work_button = document.getElementById("graphic_design_work_button");
let web_design_work_button = document.getElementById("web_design_work_button");
let landing_pages_work_button = document.getElementById("landing_pages_work_button");
let wordpress_work_button = document.getElementById("wordpress_work_button");

let all_button_imgs = document.getElementsByClassName("first_twelve");
let graphic_design_button_imgs = document.getElementsByClassName("graphic_design_img");
let web_design_button_imgs = document.getElementsByClassName("web_design_img");
let landing_pages_button_imgs = document.getElementsByClassName("landing_pages_img");
let wordpress_button_imgs = document.getElementsByClassName("wordpress_img");

let i;
let j;
let l;
let k;
let p;
let m;
let n;

all_work_button.addEventListener("click", ()=>{
    for (i=0; i<all_button_imgs.length; i++) {
        all_button_imgs[i].style.display = "block";
        };
    for (j=0; j<second_twelve.length; j++){
        second_twelve[j].style.display = "none";
    };
    load_more_button.style.display = "inline-block"
});
graphic_design_work_button.addEventListener("click", ()=>{
    for(k=0; k<graphic_design_button_imgs.length; k++){
        graphic_design_button_imgs[k].style.display="block";
    };
    for(p=0; p<web_design_button_imgs.length; p++){
        web_design_button_imgs[p].style.display="none";
    };
    for(l=0; l<landing_pages_button_imgs.length; l++){
        landing_pages_button_imgs[l].style.display="none";
    };
    for(m=0; m<wordpress_button_imgs.length; m++){
        wordpress_button_imgs[m].style.display="none";
    };
    load_more_button.style.display="none";
});
web_design_work_button.addEventListener("click", ()=>{
    for(k=0; k<graphic_design_button_imgs.length; k++){
        graphic_design_button_imgs[k].style.display="none";
    };
    for(p=0; p<web_design_button_imgs.length; p++){
        web_design_button_imgs[p].style.display="block";
    };
    for(l=0; l<landing_pages_button_imgs.length; l++){
        landing_pages_button_imgs[l].style.display="none";
    };
    for(m=0; m<wordpress_button_imgs.length; m++){
        wordpress_button_imgs[m].style.display="none";
    };
    load_more_button.style.display="none";
});
landing_pages_work_button.addEventListener("click", ()=>{
    for(k=0; k<graphic_design_button_imgs.length; k++){
        graphic_design_button_imgs[k].style.display="none";
    };
    for(p=0; p<web_design_button_imgs.length; p++){
        web_design_button_imgs[p].style.display="none";
    };
    for(l=0; l<landing_pages_button_imgs.length; l++){
        landing_pages_button_imgs[l].style.display="block";
    };
    for(m=0; m<wordpress_button_imgs.length; m++){
        wordpress_button_imgs[m].style.display="none";
    };
    load_more_button.style.display="none";
});
wordpress_work_button.addEventListener("click", ()=>{
    for(k=0; k<graphic_design_button_imgs.length; k++){
        graphic_design_button_imgs[k].style.display="none";
    };
    for(p=0; p<web_design_button_imgs.length; p++){
        web_design_button_imgs[p].style.display="none";
    };
    for(l=0; l<landing_pages_button_imgs.length; l++){
        landing_pages_button_imgs[l].style.display="none";
    };
    for(m=0; m<wordpress_button_imgs.length; m++){
        wordpress_button_imgs[m].style.display="block";
    };
    load_more_button.style.display="none";
});
let load_more_button = document.getElementById("load_more");

load_more_button.addEventListener("click",()=>{
    for(n=0; n<second_twelve.length; n++){
    second_twelve[n].style.display="block";
    };
    load_more_button.style.display="none";
});
$(".slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".slider-nav"
  });
  $(".slider-nav").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".slider-for",
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    centerPadding: '0',
    arrows: true
  });
  
