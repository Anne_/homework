let push = document.getElementById("push");
let firstInput = document.createElement("input");
let secondInput = document.createElement("input");
let circle = document.createElement("div");
let draw = document.createElement("input");
firstInput.type = "text";
firstInput.placeholder = "Enter diameter";
secondInput.type = "text";
secondInput.placeholder = "Enter color";
draw.type = "button";
draw.value = "Нарисовать";
circle.style.borderRadius = `${50}%`;
push.addEventListener("click",()=>{
    push.remove(); 
    document.body.appendChild(firstInput);
    document.body.appendChild(secondInput);
    document.body.appendChild(draw);
});
draw.addEventListener("click",()=>{
    circle.style.width = `${firstInput.value}px`;
    circle.style.height = `${firstInput.value}px`;
    circle.style.backgroundColor = `${secondInput.value}`;
    document.body.appendChild(circle);
});